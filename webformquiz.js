(function ($) {

Drupal.behaviors.webformquizFieldsetSummary = {
  attach: function (context) {

    $('fieldset.webformquiz-node-fieldset', context).drupalSetSummary(function (context) {
      if ($('.form-item-webformquiz-settings-is-quiz input').is(':checked')) {
				var validation = $('.form-item-webformquiz-settings-validation-method select option:selected').text() || 'No';
        return Drupal.t('Quiz') + ', ' + Drupal.t('@method validation', {'@method': validation});
			} else {
        return Drupal.t('Not a Quiz');
			}
    });

	}
};

})(jQuery);
